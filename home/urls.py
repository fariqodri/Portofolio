from django.conf.urls import url
from .views import index, add_suggestion

urlpatterns = [
    url(r"^$", index, name="home"),
    url(r"^add_suggestion", add_suggestion, name="suggestion"),
]
