from django.forms import ModelForm, Textarea, TextInput
from django.utils.safestring import mark_safe

from .models import Suggestion
from django.core.exceptions import NON_FIELD_ERRORS

class Suggestion(ModelForm):
    class Meta:
        model = Suggestion
        fields = ['name', 'suggestion']
        error_messages = {
            "suggestion_empty" : "Suggestion cannot be empty."
        }
        labels = {
            'name' : "Name",
            'suggestion' : 'Suggestion(s)'
        }

        widgets = {
            'name' : TextInput(attrs = {'placeholder' : 'Fill it with your name, an alias, or none', 'class' : 'form-control'}),
            'suggestion' : Textarea(attrs= {'placeholder' : 'Tell me about what you feel regarding this website or me', 'class' : 'form-control suggestion-form'})
        }