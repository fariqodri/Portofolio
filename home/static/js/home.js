// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

function moveJs() {
    var elemJs = document.getElementById("jsBar");
    var textJs = document.getElementById("jsText");
    var width = 1;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 70) {
            clearInterval(id);
        } else {
            width++;
            textJs.innerHTML = width / 10;
            elemJs.style.width = width + '%';
        }
    }
}

function moveDjango() {
    var elemDjango = document.getElementById("djangoBar");
    var textDjango = document.getElementById("djangoText");
    var width = 1;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 80) {
            clearInterval(id);
        } else {
            width++;
            textDjango.innerHTML = width / 10;
            elemDjango.style.width = width + '%';
        }
    }
}

function moveJava() {
    var elemJava = document.getElementById("javaBar");
    var textJava = document.getElementById("javaText");
    var width = 1;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 75) {
            clearInterval(id);
        } else {
            width++;
            textJava.innerHTML = width / 10;
            elemJava.style.width = width + '%';
        }
    }
}

function moveHtml() {
    var elem = document.getElementById("htmlBar");
    var textHtml = document.getElementById("htmlText");
    var width = 1;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 70) {
            clearInterval(id);
        } else {
            width++;
            textHtml.innerHTML = width / 10;
            elem.style.width = width + '%';
        }
    }
}

function moveLaravel() {
    var elem = document.getElementById("laravelBar");
    var textPython = document.getElementById("laravelText");
    var width = 1;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 80) {
            clearInterval(id);
        } else {
            width++;
            textPython.innerHTML = width / 10;
            elem.style.width = width + '%';
        }
    }
}

function move() {
    moveDjango();
    moveJava();
    moveHtml();
    moveLaravel();
    moveJs();
}
