from django.contrib import admin
from .models import Suggestion
# Register your models here.
class SuggestionAdmin(admin.ModelAdmin):
    list_display = ('name', 'suggestion', 'date')


admin.site.register(Suggestion, SuggestionAdmin)
admin.site.site_header = "Qortofolio Admin"