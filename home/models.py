from django.db import models

# Create your models here.
class Suggestion(models.Model):
    name = models.CharField(max_length=10, blank=True)
    suggestion = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
