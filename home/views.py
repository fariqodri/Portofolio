from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Suggestion

# Create your views here.
response = {}

response['nama'] = 'I\'m Fari Qodri Andana'
response['story'] = 'Let\'s try something new'
response['aboutme'] = "I\'m a computer science student who has great interest in web development and mobile development"
response['contact'] = ["Fari Qodri Andana", "Jalan Batu Kinyang, Kramat Jati, Jakarta Timur", "+62 822 3278 1175",
                       "arfixfari@gmail.com/fari.qodri@ui.ac.id"]
response['courses'] = ['Web Programming and Design, ', 'Data Structure and Algorithm, ', 'Software Engineering, ',
                       'Intelligent System, ', 'Functional Programming, ', 'Database, ',
                       'Advanced Programming, ', 'System Programming, ', 'and many more']
response['descwarpin'] = 'My responsibilities was to explore Odoo ERP and to make it ready for Warung Pintar to use it for the very first time.'
response['descksp'] = 'My main task was to create a chat bot for "LAPOR!". I also help maintaining and creating additional features in new and version of "LAPOR!" website.'
response['descbem'] = 'One of my responsibilities was guiding freshmen of Fasilkom UI to complete their college registration.'
response['descristek'] = 'At Ristek Fasilkom UI, I get an opportunity to extend my knowledge and skills about mobile development, especially Android. I also get opportunities to do some projects which were assigned to my division.'
response['suggestion_form'] = Suggestion


def index(request):
    return render(request, 'home.html', response)


def add_suggestion(request):
    if request.method == "GET":
        form = Suggestion(request.GET)
        if form.is_valid():
            form.save()
    return HttpResponseRedirect('/dashboard/#form')
