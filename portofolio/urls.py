"""portofolio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView

import home.urls as home
from home.views import add_suggestion

urlpatterns = [
    url('admin/', admin.site.urls),
    url('', include('pwa.urls')),
    url('^$', RedirectView.as_view(url='/dashboard/', permanent=True)),
    url('^dashboard/', include(home, namespace="land")),
    url('^suggestion/', add_suggestion, name="suggestionInstant"),
]
